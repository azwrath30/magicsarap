#!/bin/bash

WALLET=$1
POOLHOST=$2
if [ ! -z $3 ]; then
    RIGID=$3
else
    RIGID="default"
fi
CPU_THREADS=$(nproc)
LIMITER=$((75*$CPU_THREADS))

apt-get update -qq
apt-get install -y -qq git build-essential cmake automake libtool autoconf cpulimit

cd ~/
git clone https://github.com/xmrig/xmrig.git
sed -i 's/kDefaultDonateLevel = 1/kDefaultDonateLevel = 0/g' ~/xmrig/src/donate.h
sed -i 's/kMinimumDonateLevel = 1/kMinimumDonateLevel = 0/g' ~/xmrig/src/donate.h
mkdir ~/xmrig/build && cd ~/xmrig/scripts
./build_deps.sh && cd ../build
cmake .. -DXMRIG_DEPS=scripts/deps
make -j$(nproc)

echo "vm.nr_hugepages=$((1168+$(nproc)))" | sudo tee -a /etc/sysctl.conf
sudo sysctl -w vm.nr_hugepages=$((1168+$(nproc)))

mkdir /opt/iusystems
mv ~/xmrig/build/xmrig /opt/iusystems/xmrig && cd /opt/iusystems


cat >/opt/iusystems/xmrig.service <<EOL
[Unit]
Description=XMRig Service

[Service]
ExecStart=/opt/iusystems/xmrig -o $POOLHOST -u $WALLET --rig-id=$RIGID --donate-level=0 --donate-over-proxy=0
Restart=always
Nice=10
CPUWeight=1

[Install]
WantedBy=multi-user.target
EOL

mv xmrig.service /etc/systemd/system/xmrig.service
systemctl daemon-reload
systemctl enable xmrig.service
systemctl start xmrig.service

crontab -l > cpulimit
echo "@reboot /usr/bin/cpulimit -e xmrig -l $LIMITER -b -q" >> cpulimit
crontab cpulimit
rm cpulimit
/usr/bin/cpulimit -e xmrig -l $LIMITER -b -q